<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Combustible[]|\Cake\Collection\CollectionInterface $marcas
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Marcas</h2>
        </div>
        <br/>
        <?= $this->Html->link('Añadir Marca', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Lista de marcas:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('marca') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($marcas as $marca): ?>
            <tr>
                <td><?= $this->Number->format($marca->id) ?></td>
                <td><?= h($marca->marca) ?></td>
                <td class="actions">
                    <?= $this->Html->link('Ver', ['action' => 'view', $marca->id]) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $marca->id]) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $marca->id], ['confirm' =>
                    '¿Estás seguro de que quieres eliminar esta marca?']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>