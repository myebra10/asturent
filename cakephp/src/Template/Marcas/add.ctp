<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Modelo $marca
 */
?>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->create($marca) ?>
        <fieldset>
            <legend>Añadir Marca</legend>
            <?php echo $this->Form->control('marca');?>
        </fieldset>
        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>
    </div>
</div>