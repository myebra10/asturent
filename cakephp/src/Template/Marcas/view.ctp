<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Marca $marca
 */
?>

<div class="row">
    <div class="col-md-12">
        <h5>Datos de la marca:</h5>
        <p><strong>Id:</strong> <?= $this->Number->format($marca->id) ?></p>
        <p><strong>Marca:</strong> <?= h($marca->marca) ?></p>

        <br/>
        <?= $this->Html->link('Editar Marca', ['action' => 'edit', $marca->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Marca', ['action' => 'delete', $marca->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar esta marca?']) ?>
        <?= $this->Html->link('Lista de Marcas', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>