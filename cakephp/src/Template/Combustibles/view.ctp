<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Combustible $combustible
 */
?>

<div class="row">
    <div class="col-md-12">
        <h5>Datos del combustible:</h5>
        <p><strong>Id:</strong> <?= $this->Number->format($combustible->id) ?></p>
        <p><strong>Combustible:</strong> <?= h($combustible->combustible) ?></p>

        <br/>
        <?= $this->Html->link('Editar Combustible', ['action' => 'edit', $combustible->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Combustible', ['action' => 'delete', $combustible->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar este combustible?']) ?>
        <?= $this->Html->link('Lista de Combustibles', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>