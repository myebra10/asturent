<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Combustible[]|\Cake\Collection\CollectionInterface $combustibles
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Combustibles</h2>
        </div>
        <br/>
        <?= $this->Html->link('Añadir Combustible', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Lista de combustibles:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('combustible') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($combustibles as $combustible): ?>
            <tr>
                <td><?= $this->Number->format($combustible->id) ?></td>
                <td><?= h($combustible->combustible) ?></td>
                <td class="actions">
                    <?= $this->Html->link('Ver', ['action' => 'view', $combustible->id]) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $combustible->id]) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $combustible->id], ['confirm' =>
                    '¿Estás seguro de que quieres eliminar este combustible?']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>