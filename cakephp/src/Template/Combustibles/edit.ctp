<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Combustible $combustible
 */
?>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->create($combustible) ?>
        <fieldset>
            <legend><?= 'Editar Combustible' ?></legend>
            <?php
            echo $this->Form->control('combustible');
            ?>
        </fieldset>
        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>
    </div>
</div>