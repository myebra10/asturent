<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $coche
 */
?>

<div class="row">
    <div class="col-md-12">
        <h5>Datos del vehículo:</h5>
        <p><strong>Id:</strong> <?= h($coche->id) ?></p>
        <p><strong>Bastidor:</strong> <?= $this->Number->format($coche->precio_alquiler) ?></p>
        <p><strong>Matrícula:</strong> <?= h($coche->matricula) ?></p>
        <p><strong>Año:</strong> <?= h($coche->anyo) ?></p>
        <p><strong>Combustible:</strong> <?= h($coche->combustible->combustible) ?></p>
        <p><strong>Color:</strong> <?= h($coche->color) ?></p>
        <p><strong>Num Puertas:</strong> <?= $this->Number->format($coche->n_puertas) ?></p>
        <p><strong>Carrocería:</strong> <?= h($coche->carroceria->carroceria) ?></p>
        <p><strong>Marca:</strong> <?= h($coche->marcas_modelo->marca->marca) ?></p>
        <p><strong>Modelo:</strong> <?= h($coche->marcas_modelo->modelo->modelo) ?></p>
        <p><strong>Precio Alquiler:</strong> <?= $this->Number->format($coche->precio_alquiler) ?></p>

        <br/>
        <?= $this->Html->link('Editar Vehículo', ['action' => 'edit', $coche->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Vehículo', ['action' => 'delete', $coche->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar este vehículo?']) ?>
        <?= $this->Html->link('Lista de Vehículos', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>