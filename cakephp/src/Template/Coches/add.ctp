<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $coche
 */
?>
<div class="row">
    <?= $this->Form->create($coche) ?>
    <fieldset>
        <legend>Añadir Vehículo</legend>

        <p>Si en los desplegables no se muestran los datos requeridos, por favor, añadalos desde los siguientes
            botones:</p>
        <?= $this->Html->link('Añadir Combustibles', ['controller' => 'combustibles', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= $this->Html->link('Añadir Carrocerías', ['controller' => 'carrocerias', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= $this->Html->link('Añadir Marca - Modelo', ['controller' => 'marcasModelos', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>
        <br/>
        <br/>

        <div class="row">
            <div class="col-md-4">
                <?= $this->Form->control('bastidor'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('matricula'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('anyo'); ?>
            </div>
            <div class="col-md-4">

                <?= $this->Form->label('Combustible'); ?>
                <?= $this->Form->select('id_combustible', $combustibles, ['default' => $coche->id_combustible]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('color'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('n_puertas',['label' => 'Num Puertas']); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->label('Carroceria'); ?>
                <?= $this->Form->select('id_carroceria', $carrocerias, ['default' => $coche->id_carroceria]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->label('Marca - Modelo'); ?>
                <?= $this->Form->select('id_marca_modelo', $marcasModelos, ['default' => $coche->id_marca_modelo]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('precio_alquiler'); ?>
            </div>
        </div>
    </fieldset>

    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>