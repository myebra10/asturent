<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $coches
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Vehículos</h2>
        </div>
        <br/>
        <?= $this->Html->link('Añadir Vehículo', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Lista de Vehículos:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Bastidor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Matricula') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Año') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Combustible') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Color') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Num Puertas') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Carroceria') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Marca') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Modelo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Precio Alquiler') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($coches as $coche): ?>
            <tr>
                <td><?= $this->Number->format($coche->id) ?></td>
                <td><?= h($coche->bastidor) ?></td>
                <td><?= h($coche->matricula) ?></td>
                <td><?= h($coche->anyo) ?></td>
                <td><?= h($coche->combustible->combustible) ?></td>
                <td><?= h($coche->color) ?></td>
                <td><?= $this->Number->format($coche->n_puertas) ?></td>
                <td><?= h($coche->carroceria->carroceria) ?></td>
                <td><?= h($coche->marcas_modelo->marca->marca) ?></td>
                <td><?= h($coche->marcas_modelo->modelo->modelo) ?></td>
                <td><?= $this->Number->format($coche->precio_alquiler) ?></td>
                <td class="actions">
                    <?= $this->Html->link('Ver', ['action' => 'view', $coche->id]) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $coche->id]) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $coche->id], ['confirm' => '¿Estás seguro de que quieres eliminar el vehículo?']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>