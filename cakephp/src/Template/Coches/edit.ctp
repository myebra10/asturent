<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $coch
 */
?>
<div class="row">
    <?= $this->Form->create($coche) ?>
    <fieldset>
        <legend>Editar Vehículo</legend>
        <div class="row">
            <div class="col-md-4">
                <?= $this->Form->control('bastidor'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('matricula'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('anyo'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->label('Combustible'); ?>
                <?= $this->Form->select('id_combustible', $combustibles, ['default' => $coche->id_combustible]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('color'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('n_puertas'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->label('Carroceria'); ?>
                <?= $this->Form->select('id_carroceria', $carrocerias, ['default' => $coche->id_carroceria]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->label('Marca - Modelo'); ?>
                <?= $this->Form->select('id_marca_modelo', $marcasModelos, ['default' => $coche->id_marca_modelo]); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('precio_alquiler'); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>
