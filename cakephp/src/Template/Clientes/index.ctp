<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cliente[]|\Cake\Collection\CollectionInterface $clientes
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Clientes</h2>
        </div>
        <br/>
        <?= $this->Html->link('Añadir Cliente', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Lista de clientes:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apellidos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('d_identidad') ?></th>
                <th scope="col"><?= $this->Paginator->sort('telefono') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($clientes as $cliente): ?>
            <tr>
                <td><?= $this->Number->format($cliente->id) ?></td>
                <td><?= h($cliente->nombre) ?></td>
                <td><?= h($cliente->apellidos) ?></td>
                <td><?= h($cliente->d_identidad) ?></td>
                <td><?= h($cliente->telefono) ?></td>
                <td class="actions">
                    <?= $this->Html->link('Ver', ['action' => 'view', $cliente->id]) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $cliente->id]) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $cliente->id], ['confirm' => '¿Estás seguro de que quieres eliminar este cliente?']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>
