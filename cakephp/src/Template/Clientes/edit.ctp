<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cliente $cliente
 */
?>
<div class="row">
    <?= $this->Form->create($cliente) ?>
    <fieldset>
        <legend>Editar Cliente</legend>
        <div class="row">
            <div class="col-md-4">
                <?= $this->Form->control('nombre'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('apellidos'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('d_identidad', ['label' => 'Doc Identidad']); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('direccion'); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->control('telefono'); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>