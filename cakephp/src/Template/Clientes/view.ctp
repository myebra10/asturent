<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cliente $cliente
 */
?>
<div class="row">
    <div class="col-md-12">
        <h5>Datos del Cliente:</h5>
        <p><strong>Id:</strong> <?= h($cliente->id) ?></p>
        <p><strong>Nombre:</strong> <?= h($cliente->nombre) ?></p>
        <p><strong>Apellidos:</strong> <?= h($cliente->apellidos) ?></p>
        <p><strong>Documento de Identidad:</strong> <?= h($cliente->d_identidad) ?></p>
        <p><strong>Teléfono:</strong> <?= h($cliente->telefono) ?></p>
        <p><strong>Dirección:</strong> <?= $this->Text->autoParagraph(h($cliente->direccion)); ?></p>

        <br/>
        <?= $this->Html->link('Editar Clientes', ['action' => 'edit', $cliente->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Clientes', ['action' => 'delete', $cliente->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar esta cliente ?']) ?>
        <?= $this->Html->link('Lista de Clientes', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>