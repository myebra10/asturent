<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend>Añadir Usuario</legend>
            <?php
            echo $this->Form->control('username');
            echo $this->Form->control('password');
            echo $this->Form->control('type', ['options' => ['admin' => 'Administrador', 'user' => 'Usuario']]);
            ?>
        </fieldset>
        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>
</div>
