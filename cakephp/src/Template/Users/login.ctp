<div class="row justify-content-center">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create() ?>
    <div>
        <fieldset>
            <legend>Por favor, introduce tu nombre de usuario y contraseña</legend>
            <div class="row">
                <div class="col-md-8">
                    <?= $this->Form->input('username', ['label' => 'Nombre de Usuario']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?= $this->Form->input('password', ['label' => 'Contraseña']) ?>
                </div>
            </div>

        </fieldset>
        <div class="row">
            <div class="col-md-8">
                <?= $this->Form->button('Login'); ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>