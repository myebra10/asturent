<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Carroceria $carroceria
 */
?>
<div class="row">
    <div class="col-md-12">
        <h5>Datos de la carrocería:</h5>
        <p><strong>Id:</strong> <?= h($carroceria->id) ?></p>
        <p><strong>Carrocería:</strong> <?= h($carroceria->carroceria) ?></p>

        <br/>
        <?= $this->Html->link('Editar Carrocería', ['action' => 'edit', $carroceria->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Carrocería', ['action' => 'delete', $carroceria->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar esta carrocería?']) ?>
        <?= $this->Html->link('Lista de Carrocerias', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>