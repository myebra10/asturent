<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Carroceria[]|\Cake\Collection\CollectionInterface $carrocerias
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Carrocerías</h2>
        </div>
        <br/>
        <?= $this->Html->link('Añadir Carrocería', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Lista de carrocerías:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('carroceria') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($carrocerias as $carroceria): ?>
            <tr>
                <td><?= $this->Number->format($carroceria->id) ?></td>
                <td><?= h($carroceria->carroceria) ?></td>
                <td class="actions">
                    <?= $this->Html->link('Ver', ['action' => 'view', $carroceria->id]) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $carroceria->id]) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $carroceria->id], ['confirm' => '¿Estás seguro de que quieres eliminar esta carrocería?']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>