<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Carroceria $carroceria
 */
?>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->create($carroceria) ?>
        <fieldset>
            <legend>Añadir Carrocería</legend>
            <?php echo $this->Form->control('carroceria'); ?>
        </fieldset>

        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>

    </div>
</div>