<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;

?>
<div class="row">

    <div class="card layout-home">
        <i class="fa fa-file-text-o" style="font-size:60px;color:grey"></i>
        <div class="card-body">
            <h5 class="card-title">Nueva Ficha</h5>
            <p class="card-text">Crear ficha de salida de vehículo.</p>
            <?= $this->Html->link('Crear ficha salida',
                ['controller' => 'FichasAlquiler', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>

    <div class="card layout-home">
        <i class="fa fa-user-plus" style="font-size:60px;color:grey"></i>
        <div class="card-body">
            <h5 class="card-title">Nuevo Cliente</h5>
            <p class="card-text">Registrar un cliente nuevo.</p>
            <?= $this->Html->link('Crear ficha salida',
                ['controller' => 'Coches', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>

    <div class="card layout-home">
        <i class="fa fa-car" style="font-size:60px;color:grey"></i>
        <div class="card-body">
            <h5 class="card-title">Nuevo Coche</h5>
            <p class="card-text">Registrar un coche nuevo.</p>
            <?= $this->Html->link('Crear ficha salida',
                ['controller' => 'Coches', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>

</div>


