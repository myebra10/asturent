<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'AstuRent';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(['bootstrap.min.css', 'styleBootstrap.css']) ?>
    <?= $this->Html->script(['bootstrap.min.js', 'bootstrap.bundle.min.js', 'jquery.js']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<?php if (is_array($this->request->getSession()->read('Auth.User'))): ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="collapse navbar-collapse float-left" id="navbarNavDropdown">
            <?= $this->Html->link('AstuRent',
                ['controller' => 'Pages', 'action' => 'display'], ['class' => 'navbar-brand']) ?>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <?= $this->Html->link('Entradas/Salidas',
                        ['controller' => 'FichasAlquiler', 'action' => 'index'], ['class' => 'nav-link']) ?>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Vehículos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbardrop">
                        <?= $this->Html->link('Lista de Vehículos',
                            ['controller' => 'Coches', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Añadir Vehículo',
                            ['controller' => 'Coches', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
                        <hr>
                        <?= $this->Html->link('Lista de Carrocerías',
                            ['controller' => 'Carrocerias', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Añadir Carrocería',
                            ['controller' => 'Carrocerias', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
                        <hr>
                        <?= $this->Html->link('Lista de Combustibles',
                            ['controller' => 'Combustibles', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Añadir Combustible',
                            ['controller' => 'Combustibles', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
                        <hr>
                        <?= $this->Html->link('Lista de Marcas - Modelos',
                            ['controller' => 'MarcasModelos', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Añadir Marca - Modelo',
                            ['controller' => 'MarcasModelos', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Lista de Marcas',
                            ['controller' => 'Marcas', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Añadir Marca',
                            ['controller' => 'Marcas', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Lista de Modelos',
                            ['controller' => 'Modelos', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                        <?= $this->Html->link('Añadir Modelo',
                            ['controller' => 'Modelos', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
                    </div>
                </li>

                <li class="nav-item">
                    <?= $this->Html->link('Clientes',
                        ['controller' => 'Clientes', 'action' => 'index'], ['class' => 'nav-link']) ?>
                </li>


            </ul>
        </div>


        <div class="collapse navbar-collapse float-right">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <?= $this->Html->link(' Logout',
                        ['controller' => 'Users', 'action' => 'logout'], ['class' => 'nav-link fas fa-sign-out-alt']) ?>
                </li>
                <li class="nav-item">
                    <div class="navbar-text fas fa-user"><?php
                    $dataUser = $this->request->getSession()->read('Auth.User');
                    echo ' ' . $dataUser['username'];
                    ?></div>
                </li>
            </ul>
        </div>
    </nav>
<?php endif; ?>

<br/><br/>
<?= $this->Flash->render() ?>
<div class="container clearfix">
    <?= $this->fetch('content') ?>
</div>
<br/>
<br/>
<br/>
<footer>
</footer>
</body>
</html>
