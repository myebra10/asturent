<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Routing\Route\Route;

?>
<div class="row">

    <div class="card layout-home col-md-4">
        <i class="fa fa-file-text-o" style="font-size:60px;color:grey"></i>
        <div class="card-body">
            <h5 class="card-title">Nueva Ficha</h5>
            <p class="card-text">Crear ficha de salida de vehículo.</p>
            <?= $this->Html->link('Crear ficha salida',
                ['controller' => 'FichasAlquiler', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>

    <div class="card layout-home col-md-4">
        <i class="fa fa-user-plus" style="font-size:60px;color:grey"></i>
        <div class="card-body">
            <h5 class="card-title">Nuevo Cliente</h5>
            <p class="card-text">Registrar un cliente nuevo.</p>
            <?= $this->Html->link('Crear ficha salida',
                ['controller' => 'Coches', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>

    <div class="card layout-home col-md-4">
        <i class="fa fa-car" style="font-size:60px;color:grey"></i>
        <div class="card-body">
            <h5 class="card-title">Nuevo Vehículo</h5>
            <p class="card-text">Registrar un vehículo nuevo.</p>
            <?= $this->Html->link('Crear ficha salida',
                ['controller' => 'Coches', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>

</div>

<br/><br/>

<div class="row">
    <?php if(isset($allData)): ?>
    <div class="col-md-6">
        <canvas id="speedChart"></canvas>
    </div>

    <?php endif; ?>

    <?php if(isset($contadorCochesAlquilados)): ?>
    <div class="col-md-6">
        <h6>Número de veces que se ha alquilado un vehículo</h6>

        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col">Vehículo</th>
                <th scope="col">Num Veces Alquilado</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contadorCochesAlquilados as $index => $value): ?>
                <tr>
                    <td><?= h($coches[$index]) ?></td>
                    <td><?= h($value) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php endif; ?>
    </div>

</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">
    let dataArray = <?php echo json_encode($allData);?>;

    var keys = [];
    for(var key in dataArray) {
        if(dataArray.hasOwnProperty(key)) {
            keys.push(key);
        }
    }

    var values = [];
    for(var i=0; i<keys.length; i++){
        values.push(dataArray[keys[i]]);
    }

    var speedCanvas = document.getElementById("speedChart");

    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;

    var speedData = {
        labels: keys,
        datasets: [{
            label: "Vehículos Alquilados",
            data: values,
        }]
    };

    var chartOptions = {
        legend: {
            display: true,
            position: 'top',
            labels: {
                boxWidth: 80,
                fontColor: 'black'
            }
        }
    };

    var lineChart = new Chart(speedCanvas, {
        type: 'line',
        data: speedData,
        options: chartOptions
    });
</script>