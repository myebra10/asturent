<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $fichasAlquiler
 */
?>
<div class="row">
    <?= $this->Form->create($fichasAlquiler) ?>
    <fieldset>
        <legend>Editar Ficha Alquiler</legend>
        <div class="row">
            <div class="col-md-4">
                <?php
                echo $this->Form->label('Cliente');
                echo $this->Form->select('id_cliente', $clientes, ['required' => true]);
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $this->Form->label('Vehículo');
                echo $this->Form->select('id_coche', $coches, ['required' => true]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->input('fecha_alquiler', ['type' => 'date']); ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('km_alquiler'); ?>
            </div>

            <?php if($fichasAlquiler->ficha_cerrada == 1): ?>
            <div class="col-md-3">
                <?= $this->Form->input('fecha_devolucion', ['type' => 'date']); ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('km_devolucion'); ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('ficha_cerrada'); ?>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>