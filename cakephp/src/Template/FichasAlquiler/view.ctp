<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $fichasAlquiler
 */
?>
<div class="row">
    <div class="col-md-12">
        <h5>Datos de la Ficha:</h5>
        <p><strong>Id:</strong> <?= $this->Number->format($fichasAlquiler->id) ?></p>
        <p><strong>Cliente:</strong> <?= h($clientes[$fichasAlquiler->id_cliente]) ?></p>
        <p><strong>Vehículo:</strong> <?= h($coches[$fichasAlquiler->id_coche]) ?></p>
        <p><strong>Fecha Alquiler:</strong> <?= h($fichasAlquiler->fecha_alquiler) ?></p>
        <p><strong>Fecha Devolución:</strong> <?= h($fichasAlquiler->fecha_devolucion) ?></p>
        <p><strong>Km Alquiler:</strong> <?= $this->Number->format($fichasAlquiler->km_alquiler) ?></p>
        <p><strong>Km Devolución:</strong> <?= $this->Number->format($fichasAlquiler->km_devolucion) ?></p>
        <p><strong>Ficha Cerrada:</strong> <?= $fichasAlquiler->ficha_cerrada ? __('Yes') : __('No'); ?></p>

        <br/>
        <?= $this->Html->link('Editar Ficha', ['action' => 'edit', $fichasAlquiler->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Ficha', ['action' => 'delete', $fichasAlquiler->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar esta ficha de alquiler?']) ?>
        <?= $this->Html->link('Lista de Fichas', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>
