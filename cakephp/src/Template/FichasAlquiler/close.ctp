<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $fichasAlquiler
 */
?>
<div class="row">
    <?= $this->Form->create($fichasAlquiler) ?>
    <fieldset>
        <legend>Cerrar Ficha de Alquiler</legend>
        <div class="row">
            <div class="col-md-3"><p><strong>Id:</strong> <?= $this->Number->format($fichasAlquiler->id) ?></p></div>
            <div class="col-md-3"><p><strong>Cliente:</strong> <?= h($clientes[$fichasAlquiler->id_cliente]) ?></p>
            </div>
            <div class="col-md-3"><p><strong>Vehículo:</strong> <?= h($coches[$fichasAlquiler->id_coche]) ?></p></div>
            <div class="col-md-3"><p><strong>Fecha Alquiler:</strong> <?= h($fichasAlquiler->fecha_alquiler) ?></p>
            </div>
            <div class="col-md-3"><p><strong>Km
                        Alquiler:</strong> <?= $this->Number->format($fichasAlquiler->km_alquiler) ?></p></div>
            <div class="col-md-3">
                <?= $this->Form->input('fecha_devolucion', ['type' => 'date']); ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('km_devolucion'); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>