<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $fichasAlquiler
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Fichas Entrada/Salida</h2>
        </div>
        <br/>
        <?= $this->Html->link('Crear ficha de salida', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Fichas entrada/salida:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cliente') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Vehiculo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha_alquiler') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha_devolucion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('km_alquiler') ?></th>
                <th scope="col"><?= $this->Paginator->sort('km_devolucion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Estado Ficha') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fichasAlquiler as $fichasAlquiler): ?>
                <tr>
                    <td><?= $this->Number->format($fichasAlquiler->id) ?></td>
                    <td><?= h($clientes[$fichasAlquiler->id_cliente]) ?></td>
                    <td><?= h($coches[$fichasAlquiler->id_coche]) ?></td>
                    <td><?= h($fichasAlquiler->fecha_alquiler) ?></td>
                    <td><?= h($fichasAlquiler->fecha_devolucion) ?></td>
                    <td><?= $this->Number->format($fichasAlquiler->km_alquiler) ?></td>
                    <td><?= $this->Number->format($fichasAlquiler->km_devolucion) ?></td>
                    <td><?= $fichasAlquiler->ficha_cerrada == 1 ? 'cerrada' : 'abierta' ?></td>
                    <td class="actions">
                        <?= $this->Html->link('Ver', ['action' => 'view', $fichasAlquiler->id]) ?>
                        <?= $this->Html->link('Editar', ['action' => 'edit', $fichasAlquiler->id]) ?>
                        <?php if($fichasAlquiler->ficha_cerrada == 0) echo $this->Html->link('Cerrar Ficha', ['action' => 'close', $fichasAlquiler->id]) ?>
                        <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $fichasAlquiler->id],
                            ['confirm' => '¿Estás seguro de que quieres eliminar esta ficha de alquiler?']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>