<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $fichasAlquiler
 */
?>
<div class="row">
    <?= $this->Form->create($fichasAlquiler) ?>
    <fieldset>
        <legend>Añadir Fichas de Salida</legend>

        <p>Si en los desplegables no se muestran los datos requeridos, por favor, añadalos desde los siguientes botones:</p>
        <?= $this->Html->link('Añadir Cliente', ['controller'=> 'clientes', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= $this->Html->link('Añadir Vehículo', ['controller'=> 'coches', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>
        <br/><br/>

        <div class="row">
            <div class="col-md-4">
                <?php
                echo $this->Form->label('Cliente');
                echo $this->Form->select('id_cliente', $clientes, ['required' => true]);
                ?>
            </div>
            <div class="col-md-4">
                <?php
                echo $this->Form->label('Vehículo');
                echo $this->Form->select('id_coche', $coches, ['required' => true]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->input('fecha_alquiler', ['type' => 'date']); ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('km_alquiler', ['required' => true]); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button('Guardar') ?>
    <?= $this->Form->end() ?>
</div>
