<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $marcasModelo
 */
?>
<div class="row">
    <div class="col-md-12">
        <h5>Datos de la marca - modelo:</h5>
        <p><strong>Id:</strong> <?= h($marcasModelo->id) ?></p>
        <p><strong>Marca:</strong> <?= $marcasModelo->has('marca') ? $this->Html->link($marcasModelo->marca->marca, ['controller' => 'Marcas', 'action' => 'view', $marcasModelo->marca->id]) : '' ?></p>
        <p><strong>Modelo:</strong> <?= $marcasModelo->has('modelo') ? $this->Html->link($marcasModelo->modelo->modelo, ['controller' => 'Modelos', 'action' => 'view', $marcasModelo->modelo->id]) : '' ?></p>

        <br/>
        <?= $this->Html->link('Editar Marca - Modelo', ['action' => 'edit', $marcasModelo->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Marca - Modelo', ['action' => 'delete', $marcasModelo->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar esta marca - modelo?']) ?>
        <?= $this->Html->link('Lista de Marca - Modelo', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>