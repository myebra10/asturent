<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $marcasModelo
 */
?>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->create($marcasModelo) ?>
        <fieldset>
            <legend>Editar Marca - Modelo</legend>
            <?php
            echo $this->Form->control('id_marca', ['options' => $marcas]);
            echo $this->Form->control('id_modelo', ['options' => $modelos]);
            ?>
        </fieldset>
        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>
    </div>
</div>