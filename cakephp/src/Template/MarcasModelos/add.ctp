<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $marcasModelo
 */
?>
<div class="row">
    <div class="col-md-6">
        <br/>

        <?= $this->Form->create($marcasModelo) ?>
        <fieldset>
            <legend>Añadir Marca - Modelo</legend>

            <p>Si en los desplegables no se muestran las marcas y modelos requeridos, por favor, añadalos desde los siguientes botones:</p>
            <?= $this->Html->link('Añadir Marca', ['controller'=> 'marcas', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>
            <?= $this->Html->link('Añadir Modelo', ['controller'=> 'modelos', 'action' => 'add'], ['class' => 'btn btn-info btn-sm']) ?>

            <br/><br/>

            <?= $this->Form->label('Marca'); ?>
            <?= $this->Form->select('id_marca', $marcas); ?>
            <br/>
            <?= $this->Form->label('Modelo'); ?>
            <?= $this->Form->select('id_modelo', $modelos); ?>
        </fieldset>
        <br/>
        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>
    </div>
</div>
