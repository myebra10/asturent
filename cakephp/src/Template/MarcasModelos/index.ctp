<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $marcasModelos
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Marcas - Modelos</h2>
        </div>
        <br/>
        <?= $this->Html->link('Añadir Marca - Modelo', ['action' => 'add'], ['class' => 'btn btn-info']) ?>

        <br/><br/>

        <h5>Lista de marcas - modelos:</h5>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_marca') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_modelo') ?></th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($marcasModelos as $marcasModelo): ?>
                <tr>
                    <td><?= $this->Number->format($marcasModelo->id) ?></td>
                    <td><?= $marcasModelo->has('marca') ? $this->Html->link($marcasModelo->marca->marca, ['controller' => 'Marcas', 'action' => 'view', $marcasModelo->marca->id]) : '' ?></td>
                    <td><?= $marcasModelo->has('modelo') ? $this->Html->link($marcasModelo->modelo->modelo, ['controller' => 'Modelos', 'action' => 'view', $marcasModelo->modelo->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link('Ver', ['action' => 'view', $marcasModelo->id]) ?>
                        <?= $this->Html->link('Editar', ['action' => 'edit', $marcasModelo->id]) ?>
                        <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $marcasModelo->id], ['confirm' => __('¿Estás seguro de que quieres eliminar esta marca - modelo?')]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . 'primera') ?>
                <?= $this->Paginator->prev('< ' . 'anterior') ?>
                <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                <?= $this->Paginator->next(' siguiente ' . ' >') ?>
                <?= $this->Paginator->last(' última ' . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => 'Página {{page}} de {{pages}}, mostrando {{current}}
                registro(s) de un total de {{count}}.']) ?></p>
        </div>
    </div>
</div>