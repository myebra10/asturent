<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Modelo $modelo
 */
?>
<div class="row">
    <div class="col-md-6">
        <?= $this->Form->create($modelo) ?>
        <fieldset>
            <legend><?= 'Editar Modelo' ?></legend>
            <?php echo $this->Form->control('modelo'); ?>
        </fieldset>
        <?= $this->Form->button('Guardar') ?>
        <?= $this->Form->end() ?>
    </div>
</div>