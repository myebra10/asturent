<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Modelo $modelo
 */
?>

<div class="row">
    <div class="col-md-12">
        <h5>Datos del modelo:</h5>
        <p><strong>Id:</strong> <?= $this->Number->format($modelo->id) ?></p>
        <p><strong>Modelo:</strong> <?= h($modelo->modelo) ?></p>

        <br/>
        <?= $this->Html->link('Editar Modelo', ['action' => 'edit', $modelo->id], ['class' => 'btn btn-info']) ?>
        <?= $this->Form->postLink('Eliminar Modelo', ['action' => 'delete', $modelo->id], ['class' => 'btn btn-info', 'confirm' => '¿Estás seguro de que quieres eliminar este modelo?']) ?>
        <?= $this->Html->link('Lista de Modelos', ['action' => 'index'], ['class' => 'btn btn-info']) ?>
    </div>
</div>