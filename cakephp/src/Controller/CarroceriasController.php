<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Carrocerias Controller
 *
 * @property \App\Model\Table\CarroceriasTable $Carrocerias
 *
 * @method \App\Model\Entity\Carroceria[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CarroceriasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $carrocerias = $this->paginate($this->Carrocerias);

        $this->set(compact('carrocerias'));
    }

    /**
     * View method
     *
     * @param string|null $id Carroceria id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $carroceria = $this->Carrocerias->get($id, [
            'contain' => []
        ]);

        $this->set('carroceria', $carroceria);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $carroceria = $this->Carrocerias->newEntity();
        if ($this->request->is('post')) {
            $carroceria = $this->Carrocerias->patchEntity($carroceria, $this->request->getData());
            if ($this->Carrocerias->save($carroceria)) {
                $this->Flash->flash('La carrocería ha sido añadida correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'No se ha podido añadir la carrocería. Por favor, inténtalo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('carroceria'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Carroceria id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $carroceria = $this->Carrocerias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $carroceria = $this->Carrocerias->patchEntity($carroceria, $this->request->getData());
            if ($this->Carrocerias->save($carroceria)) {
                $this->Flash->flash('Los cambios se han guardado correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'Los cambios no se han podido guardar. Por favor, inténtelo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('carroceria'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Carroceria id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $carroceria = $this->Carrocerias->get($id);
        if ($this->Carrocerias->delete($carroceria)) {
            $this->Flash->flash('La carrocería ha sido eliminada.', ['params' => ['type' => 'success']]);
        } else {
            $this->Flash->flash(
                'No se ha podido eliminar la carrocería. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        return $this->redirect(['action' => 'index']);
    }
}
