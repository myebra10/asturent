<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Marca;
use App\Model\Table\MarcasTable;
use App\Model\Table\ModelosTable;
use Cake\ORM\TableRegistry;

/**
 * MarcasModelos Controller
 *
 * @property \App\Model\Table\MarcasModelosTable $MarcasModelos
 * @property \App\Model\Table\MarcasTable $Marcas
 * @property \App\Model\Table\ModelosTable $Modelos
 *
 * @method \App\Model\Entity\MarcasModelo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MarcasModelosController extends AppController
{
    public $Marcas;
    public $Modelos;

    public function initialize()
    {
        parent::initialize();
        $this->Marcas = TableRegistry::get('marcas');
        $this->Modelos = TableRegistry::get('modelos');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Marcas', 'Modelos']
        ];
        $marcasModelos = $this->paginate($this->MarcasModelos);

        $this->set(compact('marcasModelos'));
    }

    /**
     * View method
     *
     * @param string|null $id Marcas Modelo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $marcasModelo = $this->MarcasModelos->get($id, [
            'contain' => ['Marcas', 'Modelos']
        ]);

        $this->set('marcasModelo', $marcasModelo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $marcasModelo = $this->MarcasModelos->newEntity();
        if ($this->request->is('post')) {
            var_dump($this->request->getData());
            $data = $this->request->getData();

            $marcasModelo = $this->MarcasModelos->patchEntity($marcasModelo, $this->request->getData());
            if ($this->MarcasModelos->save($marcasModelo)) {
                $this->Flash->success(__('La marca - modelo ha sido añadida correctamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha podido añadir la marca. Por favor, inténtalo de nuevo.'));
        }

        $marcas = $this->getMarcas();
        $modelos = $this->getModelos();

        $this->set(compact('marcasModelo', 'marcas', 'modelos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Marcas Modelo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $marcasModelo = $this->MarcasModelos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $marcasModelo = $this->MarcasModelos->patchEntity($marcasModelo, $this->request->getData());
            if ($this->MarcasModelos->save($marcasModelo)) {
                $this->Flash->success(__('Los cambios se han guardado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Los cambios no se han podido guardar. Por favor, inténtelo de nuevo.'));
        }
        $marcas = $this->getMarcas();
        $modelos = $this->getModelos();

        $this->set(compact('marcasModelo', 'marcas', 'modelos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Marcas Modelo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $marcasModelo = $this->MarcasModelos->get($id);
        if ($this->MarcasModelos->delete($marcasModelo)) {
            $this->Flash->success(__('La marca - modelo ha sido eliminada.'));
        } else {
            $this->Flash->error(__('No se ha podido eliminar la marca - modelo. Por favor, inténtelo más tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getMarcas()
    {
        $marcasData =  $this->Marcas->find('all')->toArray();

        $marcas = [];
        foreach ($marcasData as $marca){
            $marcas[$marca->id] = $marca->marca;
        }
        return $marcas;
    }

    private function getModelos()
    {
        $modelosData =  $this->Modelos->find('all')->toArray();

        $modelos = [];
        foreach ($modelosData as $modelo){
            $modelos[$modelo->id] = $modelo->modelo;
        }
        return $modelos;
    }
}
