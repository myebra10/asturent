<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Modelos Controller
 *
 * @property \App\Model\Table\ModelosTable $Modelos
 *
 * @method \App\Model\Entity\Modelo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ModelosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $modelos = $this->paginate($this->Modelos);

        $this->set(compact('modelos'));
    }

    /**
     * View method
     *
     * @param string|null $id Modelo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $modelo = $this->Modelos->get($id, [
            'contain' => []
        ]);

        $this->set('modelo', $modelo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $modelo = $this->Modelos->newEntity();
        if ($this->request->is('post')) {
            $modelo = $this->Modelos->patchEntity($modelo, $this->request->getData());
            if ($this->Modelos->save($modelo)) {
                $this->Flash->flash('El modelo ha sido añadido correctamente.', ['params' => ['type' => 'success']]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'No se ha podido añadir el modelo. Por favor, inténtalo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('modelo'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Modelo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $modelo = $this->Modelos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modelo = $this->Modelos->patchEntity($modelo, $this->request->getData());
            if ($this->Modelos->save($modelo)) {
                $this->Flash->flash('Los cambios se han guardado correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'Los cambios no se han podido guardar. Por favor, inténtelo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('modelo'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Modelo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $modelo = $this->Modelos->get($id);
        if ($this->Modelos->delete($modelo)) {
            $this->Flash->flash('El modelo ha sido eliminado.', ['params' => ['type' => 'success']]);
        } else {
            $this->Flash->flash(
                'No se ha podido eliminar el modelo. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        return $this->redirect(['action' => 'index']);
    }
}