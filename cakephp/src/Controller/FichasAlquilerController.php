<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\ClientesTable;
use App\Model\Table\CochesTable;
use App\Model\Table\MarcasModelosTable;
use App\Model\Table\MarcasTable;
use App\Model\Table\ModelosTable;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * FichasAlquiler Controller
 *
 * @property \App\Model\Table\FichasAlquilerTable $FichasAlquiler
 * @property ClientesTable $Clientes
 * @property CochesTable $Coches
 * @property MarcasModelosTable $MarcasModelos
 * @property MarcasTable $Marcas
 * @property ModelosTable $Modelos
 *
 * @method \App\Model\Entity\FichasAlquiler[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FichasAlquilerController extends AppController
{
    public $Clientes;
    public $Coches;
    public $MarcasModelos;
    public $Marcas;
    public $Modelos;

    public function initialize()
    {
        parent::initialize();
        $this->Clientes = TableRegistry::get('clientes');
        $this->Coches = TableRegistry::get('coches');
        $this->MarcasModelos = TableRegistry::get('marcas_modelos');
        $this->Marcas = TableRegistry::get('marcas');
        $this->Modelos = TableRegistry::get('modelos');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $fichasAlquiler = $this->paginate($this->FichasAlquiler);
        $clientes = $this->getClientes();
        $coches = $this->getCoches();

        $this->set(compact('fichasAlquiler', 'clientes', 'coches'));
    }

    /**
     * View method
     *
     * @param string|null $id Fichas Alquiler id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fichasAlquiler = $this->FichasAlquiler->get($id, [
            'contain' => []
        ]);
        $clientes = $this->getClientes();
        $coches = $this->getCoches();

        $this->set(compact('fichasAlquiler', 'clientes', 'coches'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fichasAlquiler = $this->FichasAlquiler->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $date = new \DateTime($data['fecha_alquiler']['year'].'-'.$data['fecha_alquiler']['month'].'-'.$data['fecha_alquiler']['day']);
            $fichaAlquiler = [
                'id_cliente' => $data['id_cliente'],
                'id_coche' => $data['id_coche'],
                'fecha_alquiler' => $date,
                'km_alquiler' => $data['km_alquiler'],
                'ficha_cerrada' => false,
            ];

            $fichasAlquiler = $this->FichasAlquiler->patchEntity($fichasAlquiler, $fichaAlquiler);
            if ($this->FichasAlquiler->save($fichasAlquiler)) {
                $this->Flash->flash('La ficha de alquiler ha sido guardada.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            //var_dump($fichasAlquiler->getErrors());
            $this->Flash->flash(
                'La fichas de alquiler no se ha podido guardar. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }
        $clientes = $this->getClientes();
        $coches = $this->getCochesNoAlquilados();

        $this->set(compact('fichasAlquiler', 'clientes', 'coches'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Fichas Alquiler id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fichasAlquiler = $this->FichasAlquiler->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $dateAlquiler = new \DateTime($data['fecha_alquiler']['year'].'-'.$data['fecha_alquiler']['month'].'-'.$data['fecha_alquiler']['day']);
            $fichaAlquiler = [
                'id_cliente' => $data['id_cliente'],
                'id_coche' => $data['id_coche'],
                'fecha_alquiler' => $dateAlquiler,
                'km_alquiler' => $data['km_alquiler'],
                'ficha_cerrada' => false,
            ];

            if(isset($data['ficha_cerrada'])){
                if($data['ficha_cerrada'] == 1){
                    $dateDevolucion = new \DateTime($data['fecha_devolucion']['year'].'-'.$data['fecha_devolucion']['month'].'-'.$data['fecha_devolucion']['day']);
                    $fichaAlquiler = [
                        'id_cliente' => $data['id_cliente'],
                        'id_coche' => $data['id_coche'],
                        'fecha_alquiler' => $dateAlquiler,
                        'fecha_devolucion' => $dateDevolucion,
                        'km_alquiler' => $data['km_alquiler'],
                        'km_devolucion' => $data['km_devolucion'],
                        'ficha_cerrada' => $data['ficha_cerrada'],
                    ];
                }
            }

            $fichasAlquiler = $this->FichasAlquiler->patchEntity($fichasAlquiler, $fichaAlquiler);
            if ($this->FichasAlquiler->save($fichasAlquiler)) {
                $this->Flash->flash('Se han guardado los cambios en la fichas de alquiler.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'No se ha podido guardar los cambios en la ficha de alquiler. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        $clientes = $this->getClientes();
        $coches = $this->getCoches();

        $this->set(compact('fichasAlquiler', 'clientes', 'coches'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Fichas Alquiler id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fichasAlquiler = $this->FichasAlquiler->get($id);
        if ($this->FichasAlquiler->delete($fichasAlquiler)) {
            $this->Flash->flash('La fichas de alquiler se ha elimiando correctamente.', ['params' => ['type' => 'success']]);
        } else {
            $this->Flash->flash(
                'La fichas de alquiler no se ha podido eliminar. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        return $this->redirect(['action' => 'index']);
    }

    public function close($id = null)
    {
        $fichasAlquiler = $this->FichasAlquiler->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $dateDevolucion = new \DateTime($data['fecha_devolucion']['year'].'-'.$data['fecha_devolucion']['month'].'-'.$data['fecha_devolucion']['day']);
            $fichaAlquiler = [
                'fecha_devolucion' => $dateDevolucion,
                'km_devolucion' => $data['km_devolucion'],
                'ficha_cerrada' => true,
            ];

            $fichasAlquiler = $this->FichasAlquiler->patchEntity($fichasAlquiler, $fichaAlquiler);
            if ($this->FichasAlquiler->save($fichasAlquiler)) {
                $this->Flash->flash('La fichas de alquiler se ha cerrado correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'La fichas de alquiler no se ha podido cerrar. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        $clientes = $this->getClientes();
        $coches = $this->getCoches();

        $this->set(compact('fichasAlquiler', 'clientes', 'coches'));
    }


    private function getClientes()
    {
        $clientesData = $this->Clientes->find('all')->toArray();
        $clientes = [];
        foreach ($clientesData as $cliente){
            $clientes[$cliente->id] = $cliente->d_identidad;
        }
        return $clientes;
    }

    private function getCoches()
    {
        $marcasModelos = $this->getMarcasModelos();
        $cochesData = $this->Coches->find('all')->toArray();

        $coches = [];
        foreach ($cochesData as $coche){
            $coches[$coche->id] = $coche->matricula . ' - ' . $marcasModelos[$coche->id_marca_modelo];
        }

        return $coches;
    }

    private function getCochesNoAlquilados()
    {
        $marcasModelos = $this->getMarcasModelos();
        $cochesData = $this->Coches->find('all')->toArray();

        foreach ($cochesData as $key => $valueCoche){
            $arrayFichas = $this->FichasAlquiler->find('all')->where(['id_coche' => $valueCoche->id])->toArray();
            foreach ($arrayFichas as $ficha){
                if($ficha['ficha_cerrada'] === false){
                    unset($cochesData[$key]);
                }
            }

        }

        $coches = [];
        foreach ($cochesData as $coche){
            $coches[$coche->id] = $coche->matricula . ' - ' . $marcasModelos[$coche->id_marca_modelo];
        }

        return $coches;
    }

    private function getMarcasModelos()
    {
        $marcasModelosData = $this->MarcasModelos->find('all')->toArray();
        $marcasData = $this->Marcas->find('all')->toArray();
        $modelosData = $this->Modelos->find('all')->toArray();

        $marcas = [];
        foreach ($marcasData as $marca){
            $marcas[$marca->id] = $marca->marca;
        }

        $modelos = [];
        foreach ($modelosData as $modelo) {
            $modelos[$modelo->id] = $modelo->modelo;
        }

        $marcasModelos = [];
        foreach ($marcasModelosData as $row){
            $marcasModelos[$row['id']] = $marcas[$row['id_marca']] .' - '. $modelos[$row['id_modelo']];
        }

        return $marcasModelos;
    }

    public function home()
    {
        $fichasData = $this->FichasAlquiler->find('all')->toArray();

        $allData = [];
        foreach ($fichasData as $fichaData){
            $date = $fichaData['fecha_alquiler'];
            $month = $date->month;
            if($month<10){
                $month = '0' . $month;
            }
            $dateYM = $date->year . '-' . $month;

            if(isset($allData[$dateYM])){
                $value = $allData[$dateYM] + 1;
                $allData[$dateYM] = $value;
            }else{
                $allData[$dateYM] = 1;
            }
        }
        ksort($allData);
        $this->set(compact('allData'));

        $contadorCochesAlquilados = [];
        foreach ($fichasData as $fichaData){
            $idCoche = $fichaData['id_coche'];

            if(isset($contadorCochesAlquilados[$idCoche])){
                $value = $contadorCochesAlquilados[$idCoche]+1;
                $contadorCochesAlquilados[$idCoche] = $value;
            }else{
                $contadorCochesAlquilados[$idCoche] = 1;
            }
        }
        arsort($contadorCochesAlquilados);
        $coches = $this->getCoches();
        $this->set(compact('coches', 'contadorCochesAlquilados'));
    }


}
