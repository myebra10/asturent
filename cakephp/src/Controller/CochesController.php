<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Coche;
use App\Model\Table\CochesTable;
use App\Model\Table\CombustiblesTable;
use Cake\ORM\TableRegistry;

/**
 * Coches Controller
 *
 * @property \App\Model\Table\CochesTable $Coches
 * @property \App\Model\Table\CombustiblesTable $Combustibles
 * @property \App\Model\Table\CarroceriasTable $Carrocerias
 * @property \App\Model\Table\MarcasModelosTable $MarcasModelos
 * @property \App\Model\Table\MarcasTable $Marcas
 * @property \App\Model\Table\ModelosTable $Modelos
 *
 * @method \App\Model\Entity\Coche[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CochesController extends AppController
{
    public $Combustibles;
    public $Carrocerias;
    public $MarcasModelos;
    public $Marcas;
    public $Modelos;

    public function initialize()
    {
        parent::initialize();
        $this->Combustibles = TableRegistry::get('combustibles');
        $this->Carrocerias = TableRegistry::get('carrocerias');
        $this->MarcasModelos = TableRegistry::get('marcas_modelos');
        $this->Marcas = TableRegistry::get('marcas');
        $this->Modelos = TableRegistry::get('modelos');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $prueba = $this->Coches->find('all')
            ->select([
                'id', 'bastidor', 'matricula', 'n_puertas',
                'color', 'anyo', 'precio_alquiler'
            ])
            ->select($this->Coches->Carrocerias)
            ->select($this->Coches->Combustibles)
            ->select($this->Coches->MarcasModelos)
            ->select($this->Coches->MarcasModelos->Marcas)
            ->select($this->Coches->MarcasModelos->Modelos)
            ->contain([
                'Carrocerias',
                'Combustibles',
                'MarcasModelos',
                'MarcasModelos.Marcas',
                'MarcasModelos.Modelos',
            ]);
        $coches = $this->paginate($prueba);

        $this->set(compact('coches'));
    }

    /**
     * View method
     *
     * @param string|null $id Coche id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $coche = $this->Coches->get($id, [
            'contain' => [
                'Carrocerias',
                'Combustibles',
                'MarcasModelos',
                'MarcasModelos.Marcas',
                'MarcasModelos.Modelos',
            ]
        ]);

        $this->set('coche', $coche);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $coche = $this->Coches->newEntity();
        $combustibles = $this->getCombustibles();
        $carrocerias = $this->getCarrocerias();
        $marcasModelos = $this->getMarcasModelos();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $cocheConBastidor = $this->Coches->find('all')->where(['bastidor' => $data['bastidor']])->first();

            if(isset($cocheConBastidor)){
                $this->Flash->flash(
                    'Ya existe un vehículo con este bastidor. Por favor, añada un vehículo nuevo.',
                    ['params' => ['type' => 'danger']]
                );
                return $this->redirect(['action' => 'add']);
            }

            $cocheConMatricula = $this->Coches->find('all')->where(['matricula' => $data['matricula']])->first();
            if(isset($cocheConMatricula)){
                $this->Flash->flash(
                    'Ya existe un vehículo con esta matrícula. Por favor, añada una matricula nueva.',
                    ['params' => ['type' => 'danger']]
                );
                return $this->redirect(['action' => 'add']);
            }

            $coch = $this->Coches->patchEntity($coche, $this->request->getData());
            if ($this->Coches->save($coche)) {
                $this->Flash->flash('El vehículo se ha añadido correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'No se ha podido añadir el vehículo. Por favor, inténtelo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('coche', 'combustibles', 'carrocerias', 'marcasModelos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Coche id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $coche = $this->Coches->get($id, [
            'contain' => [
                'Carrocerias',
                'Combustibles',
                'MarcasModelos',
                'MarcasModelos.Marcas',
                'MarcasModelos.Modelos',
            ]
        ]);

        $combustibles = $this->getCombustibles();
        $carrocerias = $this->getCarrocerias();
        $marcasModelos = $this->getMarcasModelos();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $coche = $this->Coches->patchEntity($coche, $this->request->getData());
            if ($this->Coches->save($coche)) {
                $this->Flash->flash('Los cambios se han guardado correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'Los cambios no se han podido guardar. Por favor, inténtelo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('coche', 'combustibles', 'carrocerias', 'marcasModelos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Coch id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $coche = $this->Coches->get($id);
        if ($this->Coches->delete($coche)) {
            $this->Flash->flash('El vehículo se ha sido eliminado.', ['params' => ['type' => 'success']]);
        } else {
            $this->Flash->flash(
                'No se ha podido eliminar el vehículo. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getCombustibles()
    {
        $combustiblesData = $this->Combustibles->find('all')->toArray();
        $combustibles = [];
        foreach ($combustiblesData as $combustible){
            $combustibles[$combustible->id] = $combustible->combustible;
        }
        return $combustibles;
    }

    private function getCarrocerias()
    {
        $carroceriasData = $this->Carrocerias->find('all')->toArray();
        $carrocerias = [];
        foreach ($carroceriasData as $carroceria){
            $carrocerias[$carroceria->id] = $carroceria->carroceria;
        }
        return $carrocerias;
    }

    private function getMarcasModelos()
    {
        $marcasModelosData = $this->MarcasModelos->find('all')->toArray();
        $marcasData = $this->Marcas->find('all')->toArray();
        $modelosData = $this->Modelos->find('all')->toArray();

        $marcas = [];
        foreach ($marcasData as $marca){
            $marcas[$marca->id] = $marca->marca;
        }

        $modelos = [];
        foreach ($modelosData as $modelo) {
            $modelos[$modelo->id] = $modelo->modelo;
        }

        $marcasModelos = [];
        foreach ($marcasModelosData as $row){
            $marcasModelos[$row['id']] = $marcas[$row['id_marca']] .' - '. $modelos[$row['id_modelo']];
        }

        return $marcasModelos;
    }
}
