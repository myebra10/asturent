<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Combustibles Controller
 *
 * @property \App\Model\Table\CombustiblesTable $Combustibles
 *
 * @method \App\Model\Entity\Combustible[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CombustiblesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $combustibles = $this->paginate($this->Combustibles);

        $this->set(compact('combustibles'));
    }

    /**
     * View method
     *
     * @param string|null $id Combustible id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $combustible = $this->Combustibles->get($id, [
            'contain' => []
        ]);

        $this->set('combustible', $combustible);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $combustible = $this->Combustibles->newEntity();
        if ($this->request->is('post')) {
            $combustible = $this->Combustibles->patchEntity($combustible, $this->request->getData());
            if ($this->Combustibles->save($combustible)) {
                $this->Flash->flash('El combustible ha sido añadido correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'No se ha podido añadir el combustible. Por favor, inténtalo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('combustible'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Combustible id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $combustible = $this->Combustibles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $combustible = $this->Combustibles->patchEntity($combustible, $this->request->getData());
            if ($this->Combustibles->save($combustible)) {
                $this->Flash->flash('Los cambios se han guardado correctamente.', ['params' => ['type' => 'success']]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->flash(
                'Los cambios no se han podido guardar. Por favor, inténtelo de nuevo.',
                ['params' => ['type' => 'danger']]
            );
        }
        $this->set(compact('combustible'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Combustible id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $combustible = $this->Combustibles->get($id);
        if ($this->Combustibles->delete($combustible)) {
            $this->Flash->flash('El combustible ha sido eliminado.', ['params' => ['type' => 'success']]);
        } else {
            $this->Flash->flash(
                'No se ha podido eliminar el combustible. Por favor, inténtelo más tarde.',
                ['params' => ['type' => 'danger']]
            );
        }

        return $this->redirect(['action' => 'index']);
    }
}