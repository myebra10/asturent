<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Coche Entity
 *
 * @property int $id
 * @property string $bastidor
 * @property string $matricula
 * @property int $n_puertas
 * @property string $color
 * @property int $anyo
 * @property int $id_combustible
 * @property int $id_carroceria
 * @property int $id_marca_modelo
 * @property int $precio_alquiler
 */
class Coche extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
