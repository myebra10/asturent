<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FichaAlquiler Entity
 *
 * @property int $id
 * @property int $id_cliente
 * @property int $id_coche
 * @property \Cake\I18n\FrozenTime $fecha_alquiler
 * @property \Cake\I18n\FrozenTime $fecha_devolucion
 * @property int $km_alquiler
 * @property int $km_devolucion
 * @property bool $ficha_cerrada
 */
class FichaAlquiler extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
