<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Coches Model
 *
 * @method \App\Model\Entity\Coche get($primaryKey, $options = [])
 * @method \App\Model\Entity\Coche newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Coche[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Coche|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Coche|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Coche patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Coche[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Coche findOrCreate($search, callable $callback = null, $options = [])
 */
class CochesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('coches');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Carrocerias', [
            'foreignKey' => 'id_carroceria',
            'joinType' => 'LEFT',
            'className' => 'Carrocerias'
        ]);

        $this->belongsTo('Combustibles', [
            'foreignKey' => 'id_combustible',
            'joinType' => 'LEFT',
            'className' => 'Combustibles'
        ]);

        $this->belongsTo('MarcasModelos', [
            'foreignKey' => 'id_marca_modelo',
            'joinType' => 'LEFT',
            'className' => 'MarcasModelos'
        ]);

        $this->belongsToMany('Marcas', [
            'foreignKey' => 'id_marca',
            'targetForeignKey' => 'id_marca_modelo',
            'joinTable' => 'marcas_modelos',
            'ClassName' => 'Marcas',
        ]);

        $this->belongsToMany('Modelos', [
            'foreignKey' => 'id_modelo',
            'targetForeignKey' => 'id_marca_modelo',
            'joinTable' => 'marcas_modelos',
            'ClassName' => 'Modelos',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('bastidor')
            ->maxLength('bastidor', 255)
            ->requirePresence('bastidor', 'create')
            ->notEmpty('bastidor');

        $validator
            ->scalar('matricula')
            ->maxLength('matricula', 255)
            ->requirePresence('matricula', 'create')
            ->notEmpty('matricula');

        $validator
            ->integer('n_puertas')
            ->allowEmpty('n_puertas');

        $validator
            ->scalar('color')
            ->maxLength('color', 255)
            ->allowEmpty('color');

        $validator
            ->integer('anyo')
            ->allowEmpty('anyo');

        $validator
            ->integer('id_combustible')
            ->requirePresence('id_combustible', 'create')
            ->notEmpty('id_combustible');

        $validator
            ->integer('id_carroceria')
            ->requirePresence('id_carroceria', 'create')
            ->notEmpty('id_carroceria');

        $validator
            ->requirePresence('id_marca_modelo', 'create')
            ->notEmpty('id_marca_modelo');

        $validator
            ->integer('precio_alquiler')
            ->allowEmpty('precio_alquiler');

        return $validator;
    }
}
