<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MarcasModelos Model
 *
 * @method \App\Model\Entity\MarcaModelo get($primaryKey, $options = [])
 * @method \App\Model\Entity\MarcaModelo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MarcaModelo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MarcaModelo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MarcaModelo|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MarcaModelo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MarcaModelo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MarcaModelo findOrCreate($search, callable $callback = null, $options = [])
 */
class MarcasModelosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('marcas_modelos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Marcas', [
            'foreignKey' => 'id_marca',
            'joinType' => 'LEFT',
            'className' => 'Marcas'
        ]);

        $this->belongsTo('Modelos', [
            'foreignKey' => 'id_modelo',
            'joinType' => 'LEFT',
            'className' => 'Modelos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('id_marca', 'create')
            ->notEmpty('id_marca');

        $validator
            ->requirePresence('id_modelo', 'create')
            ->notEmpty('id_modelo');

        return $validator;
    }
}
