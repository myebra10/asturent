<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FichasAlquiler Model
 *
 * @method \App\Model\Entity\FichaAlquiler get($primaryKey, $options = [])
 * @method \App\Model\Entity\FichaAlquiler newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FichaAlquiler[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FichaAlquiler|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FichaAlquiler|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FichaAlquiler patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FichaAlquiler[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FichaAlquiler findOrCreate($search, callable $callback = null, $options = [])
 */
class FichasAlquilerTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fichas_alquiler');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('id_cliente', 'create')
            ->notEmpty('id_cliente');

        $validator
            ->requirePresence('id_coche', 'create')
            ->notEmpty('id_coche');

        $validator
            ->dateTime('fecha_alquiler')
            ->requirePresence('fecha_alquiler', 'create')
            ->notEmpty('fecha_alquiler');

        $validator
            ->dateTime('fecha_devolucion')
            ->allowEmpty('fecha_devolucion');

        $validator
            ->requirePresence('km_alquiler', 'create')
            ->notEmpty('km_alquiler');

        $validator
            ->allowEmpty('km_devolucion');

        $validator
            ->boolean('ficha_cerrada')
            ->requirePresence('ficha_cerrada', 'create')
            ->notEmpty('ficha_cerrada');

        return $validator;
    }
}
