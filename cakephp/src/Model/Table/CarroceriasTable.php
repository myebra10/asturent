<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Carrocerias Model
 *
 * @method \App\Model\Entity\Carroceria get($primaryKey, $options = [])
 * @method \App\Model\Entity\Carroceria newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Carroceria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Carroceria|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Carroceria|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Carroceria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Carroceria[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Carroceria findOrCreate($search, callable $callback = null, $options = [])
 */
class CarroceriasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('carrocerias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('carroceria')
            ->maxLength('carroceria', 255)
            ->requirePresence('carroceria', 'create')
            ->notEmpty('carroceria');

        return $validator;
    }
}
