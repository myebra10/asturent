<?php
use Migrations\AbstractMigration;

class CreateCoches extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('coches');
        $table->addColumn('bastidor', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('matricula', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('n_puertas', 'integer', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('color', 'string', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('anyo', 'integer', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('id_combustible', 'integer', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('id_carroceria', 'integer', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('id_marca_modelo', 'biginteger', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('precio_alquiler', 'integer', [
            'default' => null,
            'null' => true
        ]);
        $table->addForeignKey('id_combustible', 'combustibles', 'id');
        $table->addForeignKey('id_carroceria', 'carrocerias', 'id');
        $table->addForeignKey('id_marca_modelo', 'marcas_modelos', 'id');
        $table->create();
    }

    public function down()
    {
        $this->dropTable('coches');
    }
}
