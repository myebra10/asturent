<?php
use Migrations\AbstractMigration;

class CreateUsuarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('users');
        $table->addColumn('username', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('password', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('type', 'string', [
            'default' => null,
            'null' => false
        ]);

        $table->create();
    }

    public function down()
    {
        $this->dropTable('usuarios');
    }
}
