<?php
use Migrations\AbstractMigration;

class CreateMarcasModelos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('marcas_modelos');
        $table->addColumn('id_marca', 'biginteger', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('id_modelo', 'biginteger', [
            'default' => null,
            'null' => false
        ]);
        $table->addForeignKey('id_marca', 'marcas', 'id');
        $table->addForeignKey('id_modelo', 'modelos', 'id');
        $table->create();
    }
    public function down()
    {
        $this->dropTable('marcas_modelos');
    }
}
