<?php
use Migrations\AbstractMigration;

class CreateCombustibles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {$table = $this->table('combustibles');
        $table->addColumn('combustible', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->create();
    }

    public function down()
    {
        $this->dropTable('combustibles');
    }
}
