<?php
use Migrations\AbstractMigration;

class CreateClientes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('clientes');
        $table->addColumn('nombre', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('apellidos', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('d_identidad', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('direccion', 'text', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('telefono', 'string', [
            'default' => null,
            'null' => true
        ]);
        $table->create();
    }

    public function down()
    {
        $this->dropTable('clientes');
    }
}
