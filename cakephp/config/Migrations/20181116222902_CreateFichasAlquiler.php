<?php
use Migrations\AbstractMigration;

class CreateFichasAlquiler extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('fichas_alquiler');
        $table->addColumn('id_cliente', 'biginteger', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('id_coche', 'biginteger', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('fecha_alquiler', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('fecha_devolucion', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('km_alquiler', 'biginteger', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('km_devolucion', 'biginteger', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('ficha_cerrada', 'boolean', [
            'default' => false,
        ]);
        $table->addForeignKey('id_cliente', 'clientes', 'id');
        $table->addForeignKey('id_coche', 'coches', 'id');
        $table->create();
    }

    public function down()
    {
        $this->dropTable('fichas_alquiler');
    }
}
