<?php
use Migrations\AbstractMigration;

class CreateMarcas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('marcas');
        $table->addColumn('marca', 'string', [
            'default' => null,
            'null' => false
        ]);
        $table->create();
    }

    public function down()
    {
        $this->dropTable('marcas');
    }
}
