<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FichasAlquilerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FichasAlquilerTable Test Case
 */
class FichasAlquilerTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FichasAlquilerTable
     */
    public $FichasAlquiler;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fichas_alquiler'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FichasAlquiler') ? [] : ['className' => FichasAlquilerTable::class];
        $this->FichasAlquiler = TableRegistry::getTableLocator()->get('FichasAlquiler', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FichasAlquiler);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
