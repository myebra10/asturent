<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarroceriasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarroceriasTable Test Case
 */
class CarroceriasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CarroceriasTable
     */
    public $Carrocerias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.carrocerias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Carrocerias') ? [] : ['className' => CarroceriasTable::class];
        $this->Carrocerias = TableRegistry::getTableLocator()->get('Carrocerias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Carrocerias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
